﻿/// <summary>
/// File Name:  IntegerValue.cs
/// Author:     Andrew Dean
/// Copyright:  MIT License - (c)2018
/// URL:        bitbucket.org/BarkEaterBeelz/unity3d-utility-scripts
/// 
/// Desc:       Custom persisting integer value.
/// </summary>

using UnityEngine;

namespace BarkEaterBeelz.Utility {

    [CreateAssetMenu(
        fileName = "New Integer",
        menuName = "Utility/Custom Value/Integer"
    )]
    public class IntegerValue : CustomValue {
        public int initialValue;
        public int value;

        private void Awake() {
            value = initialValue;
        }

        public override string ToString() {
            return value.ToString();
        }

        // Operator Overloading
        // Add
        public static int operator +(IntegerValue v1, IntegerValue v2) {
            return v1.value + v2.value;
        }

        public static int operator +(IntegerValue v1, int v2) {
            return v1.value + v2;
        }

        public static int operator +(int v1, IntegerValue v2) {
            return v1 + v2.value;
        }

        // Subtract
        public static int operator -(IntegerValue v1, IntegerValue v2) {
            return v1.value - v2.value;
        }

        public static int operator -(IntegerValue v1, int v2) {
            return v1.value - v2;
        }

        public static int operator -(int v1, IntegerValue v2) {
            return v1 - v2.value;
        }

        // Multiply
        public static int operator *(IntegerValue v1, IntegerValue v2) {
            return v1.value * v2.value;
        }

        public static int operator *(IntegerValue v1, int v2) {
            return v1.value * v2;
        }

        public static int operator *(int v1, IntegerValue v2) {
            return v1 * v2.value;
        }

        // Divide
        public static int operator /(IntegerValue v1, IntegerValue v2) {
            return v1.value / v2.value;
        }

        public static int operator /(IntegerValue v1, int v2) {
            return v1.value / v2;
        }

        public static int operator /(int v1, IntegerValue v2) {
            return v1 / v2.value;
        }
        
    }

}
