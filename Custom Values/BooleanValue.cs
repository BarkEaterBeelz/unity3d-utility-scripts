﻿/// <summary>
/// File Name:  BooleanValue.cs
/// Author:     Andrew Dean
/// Copyright:  MIT License - (c)2018
/// URL:        bitbucket.org/BarkEaterBeelz/unity3d-utility-scripts
/// 
/// Desc:       Custom persisting boolean value.
/// </summary>

using UnityEngine;

namespace BarkEaterBeelz.Utility {

    [CreateAssetMenu(
        fileName = "New Boolean",
        menuName = "Utility/Custom Value/Boolean"
    )]
    public class BooleanValue : CustomValue {
        public bool initialValue;
        public bool value;

        private void Awake() {
            value = initialValue;
        }

        public override string ToString() {
            return value.ToString();
        }
    }

    

}
