﻿/// <summary>
/// File Name:  CustomValue.cs
/// Author:     Andrew Dean
/// Copyright:  MIT License - (c)2018
/// URL:        bitbucket.org/BarkEaterBeelz/unity3d-utility-scripts
/// 
/// Desc:       Base class for custom persisting values. Provides grouping 
///             functionality for non-compatable types, such as a stat sheet.
/// </summary>

using UnityEngine;

namespace BarkEaterBeelz.Utility {
    
    public abstract class CustomValue : ScriptableObject {
        public abstract override string ToString();
    }

}
