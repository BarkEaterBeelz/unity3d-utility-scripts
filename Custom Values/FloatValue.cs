﻿/// <summary>
/// File Name:  FloatValue.cs
/// Author:     Andrew Dean
/// Copyright:  MIT License - (c)2018
/// URL:        bitbucket.org/BarkEaterBeelz/unity3d-utility-scripts
/// 
/// Desc:       Custom persisting float value.
/// </summary>

using UnityEngine;

namespace BarkEaterBeelz.Utility {

    [CreateAssetMenu(
        fileName = "New Float",
        menuName = "Utility/Custom Value/Float"
    )]
    public class FloatValue : CustomValue {
        public float initialValue;
        public float value;

        private void Awake() {
            value = initialValue;
        }

        public override string ToString() {
            return value.ToString();
        }

        // Operator Overloading
        // Add
        public static float operator +(FloatValue v1, FloatValue v2) {
            return v1.value + v2.value;
        }

        public static float operator +(FloatValue v1, float v2) {
            return v1.value + v2;
        }

        public static float operator +(float v1, FloatValue v2) {
            return v1 + v2.value;
        }

        // Subtract
        public static float operator -(FloatValue v1, FloatValue v2) {
            return v1.value - v2.value;
        }

        public static float operator -(FloatValue v1, float v2) {
            return v1.value - v2;
        }

        public static float operator -(float v1, FloatValue v2) {
            return v1 - v2.value;
        }

        // Multiply
        public static float operator *(FloatValue v1, FloatValue v2) {
            return v1.value * v2.value;
        }

        public static float operator *(FloatValue v1, float v2) {
            return v1.value * v2;
        }

        public static float operator *(float v1, FloatValue v2) {
            return v1 * v2.value;
        }

        // Divide
        public static float operator /(FloatValue v1, FloatValue v2) {
            return v1.value / v2.value;
        }

        public static float operator /(FloatValue v1, float v2) {
            return v1.value / v2;
        }

        public static float operator /(float v1, FloatValue v2) {
            return v1 / v2.value;
        }
    }

}
