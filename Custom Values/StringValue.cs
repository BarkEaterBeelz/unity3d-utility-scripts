﻿/// <summary>
/// File Name:  StringValue.cs
/// Author:     Andrew Dean
/// Copyright:  MIT License - (c)2018
/// URL:        bitbucket.org/BarkEaterBeelz/unity3d-utility-scripts
/// 
/// Desc:       Custom persisting string value.
/// </summary>

using UnityEngine;

namespace BarkEaterBeelz.Utility {

    [CreateAssetMenu(
        fileName = "New String",
        menuName = "Utility/Custom Value/String"
    )]
    public class StringValue : CustomValue {
        public string initialValue;
        public string value;

        private void Awake() {
            value = initialValue;
        }

        public override string ToString() {
            return value.ToString();
        }

        // Operator Overloading
        // Add (concat)
        public static string operator +(StringValue v1, StringValue v2) {
            return v1.value + v2.value;
        }

        public static string operator +(StringValue v1, string v2) {
            return v1.value + v2;
        }

        public static string operator +(string v1, StringValue v2) {
            return v1 + v2.value;
        }
        
    }

}
